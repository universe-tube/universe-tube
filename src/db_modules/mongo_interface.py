########################################################################## 
# Copyright (C) 2022 Kostya Klochko <kostya_klochko@ukr.net>             #
#                                                                        #
# This file is part of Universe Tube.                                    #
#                                                                        #
# Universe Tube is free software: you can redistribute it and/or         #
# modify it under the terms of the GNU General Public License as         #
# published by the Free Software Foundation, either version 3 of the     #
# License, or (at your option) any later version.                        #
#                                                                        #
# Universe Tube is distributed in the hope that it will be useful, but   #
# WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       #
# General Public License for more details.                               #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

"""
This module helps to connect and use the same interface as other
modules modules for its DB.
"""

import pymongo

class MongoInterface:
    """Handler of connection to DB with mongo interface. Also have function for easy use."""
    def __init__(self, server: str, database: str):
        """Initialise the connection."""
        self.client = pymongo.MongoClient(server)
        self.database = self.client[database]
        self.videosdb = self.database["videos"]

    def get_videos(self):
        """Return all videos."""
        return self.database["videos"].find()

    def add_video(self, video):
        """Add dictionary as video to MongoDB."""
        self.videosdb.insert_one(video)
