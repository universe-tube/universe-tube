########################################################################## 
# Copyright (C) 2022 Kostya Klochko <kostya_klochko@ukr.net>             #
#                                                                        #
# This file is part of Universe Tube.                                    #
#                                                                        #
# Universe Tube is free software: you can redistribute it and/or         #
# modify it under the terms of the GNU General Public License as         #
# published by the Free Software Foundation, either version 3 of the     #
# License, or (at your option) any later version.                        #
#                                                                        #
# Universe Tube is distributed in the hope that it will be useful, but   #
# WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       #
# General Public License for more details.                               #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
from db_modules import MongoInterface

def main():
    # Test connection
    server = "mongodb://localhost:2717"
    database = "db"
    mi = MongoInterface(server, database)

    # Add video
    mi.add_video({"videoId":"videoId","title":"A title."})

    # Show all videos
    for v in mi.get_videos():
        print(v)

if __name__ == "__main__":
    main()
