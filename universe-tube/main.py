##########################################################################
# Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>    #
#                                                                        #
# This file is part of Universe Tube.                                    #
#                                                                        #
# Universe Tube is free software: you can redistribute it and/or         #
# modify it under the terms of the GNU General Public License as         #
# published by the Free Software Foundation, either version 3 of the     #
# License, or (at your option) any later version.                        #
#                                                                        #
# Universe Tube is distributed in the hope that it will be useful, but   #
# WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       #
# General Public License for more details.                               #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
# This Python file uses the following encoding: utf-8
import sys
import os
from pathlib import Path

from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Video, Channel
from models import create_tables

import resources
LIBRARY_DIR = os.fspath(Path(__file__).resolve().parent / "library" / "Configuration")

if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    engine.addImportPath(LIBRARY_DIR)

    # Connect to the database
    engine_db = create_engine("sqlite:///db.db", echo=True)
    Session = sessionmaker(bind=engine_db)
    session = Session()

    # Create tables of models
    create_tables(engine_db)
    qml_file = Path(__file__).resolve().parent / "qml" / "main.qml"
    #qml_file = "qrc:///qml/qml/main.qml"
    engine.load(qml_file)
    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec())
