import QtQuick 2.15
import QtQuick.Controls 2.15
import Themes 1.0
Item {
    Rectangle {
        id: settingPage
        color: Themes.background
        anchors.fill: parent

        Label {
            id: label
            text: qsTr("Settings Page")
            anchors.centerIn: parent
            color: Themes.text
            font.pointSize: 16
        }
    }
}
