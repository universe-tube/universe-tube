////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>    //
//                                                                        //
// This file is part of Universe Tube.                                    //
//                                                                        //
// Universe Tube is free software: you can redistribute it and/or         //
// modify it under the terms of the GNU General Public License as         //
// published by the Free Software Foundation, either version 3 of the     //
// License, or (at your option) any later version.                        //
//                                                                        //
// Universe Tube is distributed in the hope that it will be useful, but   //
// WITHOUT ANY WARRANTY; without even the implied warranty of             //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       //
// General Public License for more details.                               //
//                                                                        //
// You should have received a copy of the GNU General Public License      //
// along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. //
////////////////////////////////////////////////////////////////////////////
import QtQuick 2.15
import QtQuick.Controls 2.15

Button {
    id: menuButton

    text: qsTr("Click on Me")
    implicitWidth: 200
    implicitHeight: 80

    property string buttonColor: "black"
    property string textColor: "white"
    property url btnIconSource: ""

    background: Rectangle {
        color: buttonColor
    }

    contentItem: Item {
        id: innerItem
        Text {
            id: buttonText
            text: menuButton.text
            font.pixelSize: 14
            color: textColor
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 80
            }
        }

        Image {
            id: name
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 20
            }
            height: 32
            width: 32
            source: menuButton.btnIconSource
        }
    }
}
