////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>    //
//                                                                        //
// This file is part of Universe Tube.                                    //
//                                                                        //
// Universe Tube is free software: you can redistribute it and/or         //
// modify it under the terms of the GNU General Public License as         //
// published by the Free Software Foundation, either version 3 of the     //
// License, or (at your option) any later version.                        //
//                                                                        //
// Universe Tube is distributed in the hope that it will be useful, but   //
// WITHOUT ANY WARRANTY; without even the implied warranty of             //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       //
// General Public License for more details.                               //
//                                                                        //
// You should have received a copy of the GNU General Public License      //
// along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. //
////////////////////////////////////////////////////////////////////////////
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.0
import "controls"
import "pages"
import Themes 1.0

Window {
    id: mainWindow
    width: 640
    height: 480
    visible: true
    title: qsTr("Universe Tube")

    //Colors
    property string topBarMainColor: Themes.main
    property string leftMenuMainColor: Themes.main
    property string leftMenuButtonColor: Themes.material.accent
    property string pageMainColor: Themes.background
    Material.theme: materialTheme
    Material.accent: materialAccent
    color: pageMainColor

    Rectangle {
        id: topBar
        color: mainWindow.topBarMainColor
        width: parent.width
        height: 60
        anchors {
            top: parent.top
            left: parent.left
            right: parent.rigth
        }
        visible: true

        ToggleButton {
            id: toggleButton
            anchors {
                top: parent.top
                left: parent.left
                bottom: parent.bottom
            }
            onClicked: toggleMenuAnimation.running = true
        }

        Rectangle {
            id: mainSearchBox
            color: "transparent"
            anchors {
                left: toggleButton.right
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
            Row {
                spacing: 10
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: 10
                    bottom: parent.bottom
                    bottomMargin: 10
                }

                TextField {
                    id: mainSearch
                    width: 200
                    placeholderText: qsTr("Type a url")
                    font.pixelSize: 18
                }

                Button {
                    id: mainSearchButton
                    text: qsTr("Search")
                }
            }
        }
    }

    Rectangle {
        id: leftMenu
        color: mainWindow.leftMenuMainColor
        width: 80
        height: parent.height - topBar.height
        anchors {
            top: topBar.bottom
            left: parent.left
        }
        visible: true
        clip: true

        PropertyAnimation {
            id: toggleMenuAnimation
            target: leftMenu
            property: "width"
            to: if (leftMenu.width == 80)
                    return 200
                else
                    return 80
            duration: 250
            easing.type: Easyng.InOutQuint
        }

        Column {
            id: leftMenuButtonColumn
            spacing: 5
            clip: true
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                topMargin: this.spacing
            }
            LeftMenuButton {
                text: qsTr("Home")
                buttonColor: leftMenuButtonColor
                btnIconSource: "qrc:///svg/images/svg/home.svg"
            }

            LeftMenuButton {
                text: qsTr("Favorite")
                buttonColor: leftMenuButtonColor
                btnIconSource: "qrc:///svg/images/svg/star.svg"
            }

            LeftMenuButton {
                text: qsTr("Bookmarks")
                buttonColor: leftMenuButtonColor
                btnIconSource: "qrc:///svg/images/svg/bookmark.svg"
            }

            LeftMenuButton {
                text: qsTr("History")
                buttonColor: leftMenuButtonColor
                btnIconSource: "qrc:///svg/images/svg/history.svg"
            }

            LeftMenuButton {
                id: settingsButton
                text: qsTr("Setting")
                buttonColor: leftMenuButtonColor
                btnIconSource: "qrc:///svg/images/svg/settings.svg"
            }
        }
    }

    Rectangle {
        id: pager

        anchors {
            top: topBar.bottom
            left: leftMenu.right
            right: parent.right
            bottom: parent.bottom
        }

        StackView {
            id: stackView
            anchors.fill: parent
            initialItem: SettingsPage {}
            clip: true
        }
    }
}
