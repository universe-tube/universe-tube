##########################################################################
# Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>    #
#                                                                        #
# This file is part of Universe Tube.                                    #
#                                                                        #
# Universe Tube is free software: you can redistribute it and/or         #
# modify it under the terms of the GNU General Public License as         #
# published by the Free Software Foundation, either version 3 of the     #
# License, or (at your option) any later version.                        #
#                                                                        #
# Universe Tube is distributed in the hope that it will be useful, but   #
# WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       #
# General Public License for more details.                               #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from sqlalchemy import Column, ForeignKey, Integer, String, Text, Table
from sqlalchemy.orm import relationship
from sqlalchemy.orm import registry, Mapped, mapped_column
from dataclasses import dataclass, field

mapper_registry = registry()
NAME_SIZE = 256
VIDEO_ID_SIZE = 11
AUTHOR_ID_SIZE = 24

@mapper_registry.mapped
@dataclass
class Video:
    __table__ = Table(
        "videos",
        mapper_registry.metadata,
        Column("id", Integer, primary_key=True),
        Column("title", String(NAME_SIZE)),
        Column("videoId", String(VIDEO_ID_SIZE)),
        Column("description", Text),
        Column("published", Integer),
        Column("channel_id", Integer, ForeignKey("channel.id")),
    )

    id: int = field(init=False)
    title: Mapped[str]
    videoId: Mapped[str]
    description: Mapped[str]
    published: Mapped[int]
    channel_id: Mapped[int] = field(init=False)
    channel = relationship("Channel", primaryjoin='Channel.id==Video.channel_id')

@mapper_registry.mapped
@dataclass
class Channel:
    __table__ = Table(
        "channel",
        mapper_registry.metadata,
        Column("id", Integer, primary_key=True),
        Column("author", String(NAME_SIZE)),
        Column("authorId", String(AUTHOR_ID_SIZE)),
        Column("description", Text),
    )
    id: int = field(init=False)
    author: Mapped[str]
    authorId: Mapped[str]
    description: Mapped[str]

def create_tables(engine):
    mapper_registry.metadata.create_all(engine)
