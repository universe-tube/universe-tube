////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>    //
//                                                                        //
// This file is part of Universe Tube.                                    //
//                                                                        //
// Universe Tube is free software: you can redistribute it and/or         //
// modify it under the terms of the GNU General Public License as         //
// published by the Free Software Foundation, either version 3 of the     //
// License, or (at your option) any later version.                        //
//                                                                        //
// Universe Tube is distributed in the hope that it will be useful, but   //
// WITHOUT ANY WARRANTY; without even the implied warranty of             //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       //
// General Public License for more details.                               //
//                                                                        //
// You should have received a copy of the GNU General Public License      //
// along with Universe Tube. If not, see <https://www.gnu.org/licenses/>. //
////////////////////////////////////////////////////////////////////////////
pragma Singleton
import QtQuick 2.15
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
Item {
    property alias themes: themes
    QtObject {
        id: themes
        readonly property var defaultDark: ["#2b2b2b", "#3c3c3c", "white", "grey"]
    }

    Material.theme: Material.Dark
    Material.accent: Material.Brown
    property var currTheme: themes.defaultDark
    readonly property var material: Material
    readonly property var materialTheme: Material.theme
    readonly property var materialAccent: Material.accent
    readonly property string main: currTheme[0]
    readonly property string background: Material.background//currTheme[1]
    readonly property string text: currTheme[2]

}
